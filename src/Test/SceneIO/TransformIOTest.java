package SceneIO;

import org.junit.jupiter.api.Test;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertThrows;

class TransformIOTest {

	@Test
	void read_generalException() {

		assertThrows(
			IOException.class,
			() -> TransformIO.read(new String[] {"gibberish"})
		);
	}

	@Test
	void read_singleScale() throws IOException {

		for(double scale : new double[] {1, 2, 3.2, -4.5, 0, 156.32} ) {

			String[] ss = {("scale " + scale)};
			Matrix4d mat = TransformIO.read(ss);

			assertMultiply(
				mat,
				new Point3d(1, 2, 3),
				new Point3d(1.0 * scale, 2.0 * scale, 3 * scale)
			);

			assertMultiply(
				mat,
				new Vector3d(1, 2, 3),
				new Vector3d(1.0 * scale, 2.0 * scale, 3 * scale)
			);
		}
	}

	@Test
	void read_multipleScales() throws IOException {

		String[] ss = {
			"scale 2",
			"scale 3.2",
			"scale -5.4"
		};
		double scale = 2.0*3.2*(-5.4);
		Matrix4d mat = TransformIO.read(ss);

		assertMultiply(
			mat,
			new Point3d(1, 2, 3),
			new Point3d(1.0 * scale, 2.0 * scale, 3 * scale)
		);

		assertMultiply(
			mat,
			new Vector3d(1, 2, 3),
			new Vector3d(1.0 * scale, 2.0 * scale, 3 * scale)
		);
	}

	@Test
	void read_scaleException() {

		String[] ss = {
			"scale",
			"scale ",
			"scale 1 2",
			"scale v"
		};
		for(String s : ss) {
			assertThrows(IOException.class, () -> TransformIO.read(new String[] {s}));
		}
	}

	@Test
	void read_singleTranslate() throws IOException {

		Vector3d v = new Vector3d(3, 1.5, 40.554);
		Matrix4d mat = TransformIO.read(new String[] {"translate " + v.x + " " + v.y + " " + v.z});

		assertMultiply(
			mat,
			new Point3d(1, 2, 3),
			new Point3d(1 + v.x, 2 + v.y, 3 + v.z)
		);

		assertMultiply(
			mat,
			new Vector3d(1, 2, 3),
			new Vector3d(1, 2, 3)
		);
	}

	@Test
	void read_multipleTranslates() throws IOException {

		String[] ss = {
			"translate 1 2 3",
			"translate -4.3 -0 8.8",
			"translate 2 7.7 9.33"
		};
		Vector3d v = new Vector3d(
			1 - 4.3 + 2,
			2 + 7.7,
			3 + 8.8 + 9.33
		);
		Matrix4d mat = TransformIO.read(ss);

		assertMultiply(
			mat,
			new Point3d(1, 2, 3),
			new Point3d(1 + v.x, 2 + v.y, 3 + v.z)
		);

		assertMultiply(
			mat,
			new Vector3d(1, 2, 3),
			new Vector3d(1, 2, 3)
		);
	}

	@Test
	void read_translateException() {

		String[] ss = {
			"translate",
			"translate ",
			"translate 1",
			"translate 1 2",
			"translate 1, 2, 3",
			"translate 1 2 3 4",
			"translate 1 2 s"
		};

		for(String s : ss) {
			assertThrows(IOException.class, () -> TransformIO.read(new String[] {s}));
		}
	}

	@Test
	void read_singleRotateX() throws IOException {

		Matrix4d mat = TransformIO.read(new String[] {"rotate about 1 0 0 by 90"});

		assertMultiply(
			mat,
			new Point3d(1,2,-3),
			new Point3d(1,3,2 )
		);

		assertMultiply(
			mat,
			new Vector3d(1,2,-3),
			new Vector3d(1,3,2 )
		);
	}

	@Test
	void read_singleRotateY() throws IOException {

		Matrix4d mat = TransformIO.read(new String[] {"rotate about 0 1 0 by 90"});

		assertMultiply(
			mat,
			new Point3d(1,2,-3),
			new Point3d(-3,2, -1)
		);

		assertMultiply(
			mat,
			new Vector3d(1,2,-3),
			new Vector3d(-3,2, -1)
		);
	}

	@Test
	void read_singleRotateZ() throws IOException {

		Matrix4d mat = TransformIO.read(new String[] {"rotate about 0 0 1 by 90"});

		assertMultiply(
			mat,
			new Point3d(1,2,-3),
			new Point3d(-2,1, -3)
		);

		assertMultiply(
			mat,
			new Vector3d(1,2,-3),
			new Vector3d(-2,1, -3)
		);
	}

	@Test
	void read_singleRotateDiagonal() throws IOException {

		Matrix4d mat = TransformIO.read(new String[] {"rotate about 1 1 1 by 120"});

		assertMultiply(
			mat,
			new Point3d(1,0, 0),
			new Point3d(0, 1, 0)
		);

		assertMultiply(
			mat,
			new Vector3d(1,0, 0),
			new Vector3d(0, 1, 0)
		);
	}

	@Test
	void read_multipleRotates() throws IOException {

		String[] ss = {
			"rotate about 1 0 0 by 90",
			"rotate about 0 0 1 by 90",
			"rotate about 0 1 0 by 90"
		};
		Matrix4d mat = TransformIO.read(ss);

		// (1,1,-1) -> (1,1,1) -> (-1,1,1) -> (1,1,1)

		assertMultiply(
			mat,
			new Point3d(1, 1, -1),
			new Point3d(1, 1, 1)
		);

		assertMultiply(
			mat,
			new Vector3d(1, 1, -1),
			new Vector3d(1, 1, 1)
		);
	}

	@Test
	void read_rotateException() {

		String[] ss = {
			"rotate",
			"rotate ",
			"rotate 1",
			"rotate around 1 0 0",
			"rotate by 1",
			"rotate about 1 0 0",
			"rotate about 1 0 0 by s",
			"rotate about 1 0 0 2 by 34",
			"rotate about 1 0 by 34",
			"rotate about 1 0 0 by 2 34"
		};

		for(String s : ss) {
			assertThrows(IOException.class, () -> TransformIO.read(new String[] {s}));
		}
	}

	@Test
	void read_translatesAndRotates() throws IOException {

		String[] ss = {
			"translate 1 2 3",
			"rotate about 0 0 1 by 90",
			"translate 2 7.7 9.3",
			"rotate about 0 1 0 by 90"
		};
		Matrix4d mat = TransformIO.read(ss);

		// (1,1,1) -> (2,3,4) -> (-3,2,4) -> (-1,9.7,13.3) -> (13.3,9.7,1)
		// (1,1,1) -> (1,1,1) -> (-1,1,1) -> (-1,1,1)      -> (1,1,1)

		assertMultiply(
			mat,
			new Point3d(1,1,1),
			new Point3d(13.3, 9.7, 1)
		);

		assertMultiply(
			mat,
			new Vector3d(1,1, 1),
			new Vector3d(1, 1, 1)
		);
	}

	@Test
	void read_scalesAndTranslates() throws IOException {

		String[] ss = {
			"scale 2",
			"translate 1 2 3",
			"scale 3",
			"translate 4 5 6",
		};
		Matrix4d mat = TransformIO.read(ss);

		// (6,6,6) -> (7,8,9) -> (11,13,15)
		// (6,6,6) -> (6,6,6) -> (6,6,6)

		assertMultiply(
			mat,
			new Point3d(1,1,1),
			new Point3d(11, 13, 15)
		);

		assertMultiply(
			mat,
			new Vector3d(1,1, 1),
			new Vector3d(6, 6, 6)
		);
	}

	@Test
	void read_rotatesAndScales() throws IOException {

		String[] ss = {
			"scale 2",
			"rotate about 0 0 1 by 90",
			"scale 3",
			"rotate about 0 1 0 by 90"
		};
		Matrix4d mat = TransformIO.read(ss);

		// (6,6,6) -> (-6,6,6) -> (6,6,6)
		// (6,6,6) -> (-6,6,6) -> (6,6,6)
		assertMultiply(
			mat,
			new Point3d(1, 1, 1),
			new Point3d(6, 6, 6)
		);

		assertMultiply(
			mat,
			new Vector3d(1, 1, 1),
			new Vector3d(6, 6, 6)
		);
	}

	@Test
	void read_mixedTransforms() throws IOException {

		String[] ss = {
			"translate 1 2 3",
			"scale 2",
			"rotate about 0 0 1 by 90",
			"scale 1.5",
			"translate 2 7.7 9.3",
			"rotate about 0 1 0 by 90"
		};
		Matrix4d mat = TransformIO.read(ss);

		// note: scaling is applied initially

		// (3,3,3) -> (4,5,6) -> (-5,4,6) -> (-3,11.7,15.3) -> (15.3,11.7,3)
		// (3,3,3) -> (3,3,3) -> (-3,3,3) -> (-3,3,3)       -> (3,3,3)

		assertMultiply(
			mat,
			new Point3d(1, 1, 1),
			new Point3d(15.3, 11.7, 3)
		);

		assertMultiply(
			mat,
			new Vector3d(1, 1, 1),
			new Vector3d(3, 3, 3)
		);
	}

	@Test
	void forCoverageSake() {
		new TransformIO();
	}

	private void assertMultiply(Matrix4d A, Point3d b, Point3d c) {
		Point3d t = new Point3d(b);
		A.transform(t);
		t.sub(c);
		assert(t.distance(new Point3d(0,0,0)) < 1e-5);
	}

	private void assertMultiply(Matrix4d A, Vector3d b, Vector3d c) {
		Vector3d t = new Vector3d(b);
		A.transform(t);
		t.sub(c);
		assert(t.length() < 1e-5);
	}
}