package SceneIO;

import Polyhedron.IColouredPolyhedron;
import PolyhedronBuilder.PolyhedronBuilder;
import PolyhedronIO.PolyhedronIO;
import TextIO.TextIO;
import Unfolding.IUnfoldablePolyhedron;
import org.junit.jupiter.api.Test;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ModelsIOTest {

	@Test
	void read_fromObjFile() throws IOException {

		// setup obj file content
		IUnfoldablePolyhedron cube = PolyhedronBuilder.getCube(false);
		String s1 = PolyhedronIO.write(cube);
		TextIO.write(Paths.get("Scene\\src\\Data\\TestData\\ModelsIOTest\\testModel1.obj"), s1);

		IUnfoldablePolyhedron pyramid = PolyhedronBuilder.getPyramid(false);
		String s2 = PolyhedronIO.write(pyramid);
		TextIO.write(Paths.get("Scene\\src\\Data\\TestData\\ModelsIOTest\\testModel2.obj"), s2);

		// setup input parameters
		Model[] models = new Model[2];
		models[0] = new Model();
		models[0].file = "testModel1.obj";
		models[1] = new Model();
		models[1].file = "testModel2.obj";
		Path dir = Paths.get("Scene\\src\\Data\\TestData\\ModelsIOTest\\");

		// read from file
		IColouredPolyhedron[] polyhedra = ModelsIO.read(models, dir);

		// verify content read is correct
		assertArrayEquals(
			new IColouredPolyhedron[] {cube, pyramid},
			polyhedra
		);
	}

	@Test
	void read_fromPngFile() throws IOException {

		// setup obj file content
		BufferedImage im = new BufferedImage(2, 2, BufferedImage.TYPE_INT_RGB);
		im.setRGB(0,0, Color.red.getRGB()); // top left
		im.setRGB(0,1, Color.green.getRGB()); // bottom left
		im.setRGB(1,0, Color.blue.getRGB()); // top right
		im.setRGB(1,1, Color.white.getRGB()); // bottom right
		ImageIO.write(im, "png", new File("Scene\\src\\Data\\TestData\\ModelsIOTest\\testIm.png"));

		// setup input parameters
		Model[] models = new Model[1];
		models[0] = new Model();
		models[0].file = "testIm.png";
		Path dir = Paths.get("Scene\\src\\Data\\TestData\\ModelsIOTest\\");

		// read from file
		IColouredPolyhedron[] polyhedra = ModelsIO.read(models, dir);

		// verify content read is correct
		assertArrayEquals(
			new IColouredPolyhedron[] {PolyhedronIO.read(im)},
			polyhedra
		);
	}

	@Test
	void read_fromFile_invalidExtension() throws IOException{

		// setup obj file content
		IUnfoldablePolyhedron cube = PolyhedronBuilder.getCube(false);
		String s1 = PolyhedronIO.write(cube);
		TextIO.write(Paths.get("Scene\\src\\Data\\TestData\\ModelsIOTest\\testModel1.obj"), s1);

		IUnfoldablePolyhedron pyramid = PolyhedronBuilder.getPyramid(false);
		String s2 = PolyhedronIO.write(pyramid);
		TextIO.write(Paths.get("Scene\\src\\Data\\TestData\\ModelsIOTest\\testModel2.gif"), s2);

		// setup input parameters
		Model[] models = new Model[2];
		models[0] = new Model();
		models[0].file = "testModel1.obj";
		models[1] = new Model();
		models[1].file = "testModel2.gif";
		Path dir = Paths.get("Scene\\src\\Data\\TestData\\ModelsIOTest\\");

		// read from file
		assertThrows(IOException.class, () -> ModelsIO.read(models, dir));
	}

	@Test
	void read_fromFile_fileNotFound() {

		// setup input parameters
		Model[] models = new Model[1];
		models[0] = new Model();
		models[0].file = "asdfasdf.obj";
		Path dir = Paths.get("Scene\\src\\Data\\TestData\\ModelsIOTest\\");

		// read from file
		assertThrows(IOException.class, () -> ModelsIO.read(models, dir));
	}

	@Test
	void read_fromPresets() throws IOException {

		// setup input parameters
		Model[] models = new Model[4];
		models[0] = new Model();
		models[0].shape = "sphere";
		models[1] = new Model();
		models[1].shape = "hemisphere";
		models[2] = new Model();
		models[2].shape = "cube";
		models[3] = new Model();
		models[3].shape = "pyramid";
		Path dir = Paths.get("dir\\doesnt\\matter\\for\\presets");

		// read from file
		IColouredPolyhedron[] polyhedra = ModelsIO.read(models, dir);

		// verify content read is correct
		assertArrayEquals(
			new IColouredPolyhedron[]{
				PolyhedronBuilder.getSphere(36, 12, false),
				PolyhedronBuilder.getHemisphere(36, 12, false),
				PolyhedronBuilder.getCube(false),
				PolyhedronBuilder.getPyramid(false)
			},
			polyhedra
		);
	}

	@Test
	void read_fromPresets_invalid() {

		// setup input parameters
		Model[] models = new Model[4];
		models[0] = new Model();
		models[0].shape = "sphere";
		models[1] = new Model();
		models[1].shape = "hemisphere";
		models[2] = new Model();
		models[2].shape = "pentagonalBipyramid";
		models[3] = new Model();
		models[3].shape = "pyramid";
		Path dir = Paths.get("dir\\doesnt\\matter\\for\\presets");

		// read from file
		assertThrows(IOException.class, () -> ModelsIO.read(models, dir));
	}

	@Test
	void read_emptyModels() {

		Path dir = Paths.get("dir\\doesnt\\matter\\for\\presets");

		assertThrows(IOException.class, () -> ModelsIO.read(null, dir));
		assertThrows(IOException.class, () -> ModelsIO.read(new Model[] {new Model()}, dir));
	}

	@Test
	void forCoverageSake() {
		new ModelsIO();
	}

}