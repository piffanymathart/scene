package SceneIO;

import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ViewIOTest {

	@Test
	void readFovY() throws IOException {

		assertThrows(IOException.class, () -> ViewIO.readFovY(null));

		assertThrows(IOException.class, () -> ViewIO.readFovY(new View()));

		View view = new View();
		view.fov = 45;
		assertEquals(Math.PI/4.0, ViewIO.readFovY(view), 1e-5);
	}

	@Test
	void writeFovY() throws IOException {

		assertEquals(45, ViewIO.writeFovY(Math.PI/4.0), 1e-5);
	}

	@Test
	void forCoverageSake() {
		new ViewIO();
	}
}