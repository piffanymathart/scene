package SceneIO;

import Polyhedron.IColouredPolyhedron;
import PolyhedronBuilder.PolyhedronBuilder;
import PolyhedronIO.PolyhedronIO;
import Scene.IScene;
import Scene.Scene;
import Scene.Surface;
import TextIO.TextIO;
import Unfolding.IUnfoldablePolyhedron;
import Unfolding.UnfoldablePolyhedron;
import org.junit.jupiter.api.Test;

import javax.vecmath.Matrix4d;
import javax.vecmath.Vector3d;
import java.awt.*;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SceneIOTest {

	@Test
	void read() throws IOException {

		// Setup surface obj
		IUnfoldablePolyhedron screen = PolyhedronBuilder.getSphere(24, 8, true);
		String screenStr = PolyhedronIO.write(screen);
		TextIO.write(Paths.get("Scene\\src\\Data\\TestData\\SceneIOTest\\myScreen.obj"), screenStr);

		// Setup models obj
		IUnfoldablePolyhedron model1 = PolyhedronBuilder.getPyramid(false);
		String model1Str = PolyhedronIO.write(model1);
		TextIO.write(Paths.get("Scene\\src\\Data\\TestData\\SceneIOTest\\myModel1.obj"), model1Str);

		IUnfoldablePolyhedron model2 = PolyhedronBuilder.getCube(false);
		model2 = new UnfoldablePolyhedron(
			model2.getVertices(),
			model2.getEdgeIndices(),
			model2.getFaceIndices(),
			new Color[] {Color.red, Color.orange, Color.yellow, Color.green, Color.blue, Color.magenta},
			null
		);
		String model2Str = PolyhedronIO.write(model2);
		TextIO.write(Paths.get("Scene\\src\\Data\\TestData\\SceneIOTest\\myModel2.obj"), model2Str);

		// Setup JSON scene file

		String s = "{\n" +
			"  \"view\": {\n" +
			"    \"fov\": 45.0\n" +
			"  },\n" +
			"  \"models\": [\n" +
			"    {\n" +
			"      \"file\": \"myModel1.obj\",\n" +
			"      \"transformations\": [\n" +
			"        \"translate 1 2 3\",\n" +
			"        \"scale 1\"\n" +
			"    ]" +
			"    },\n" +
			"    {\n" +
			"      \"file\": \"myModel2.obj\",\n" +
			"      \"transformations\": []\n" +
			"    }\n" +
			"  ],\n" +
			"  \"screen\": {\n" +
			"    \"file\": \"myScreen.obj\"\n" +
			"  }\n" +
			"}";

		TextIO.write(Paths.get("Scene\\src\\Data\\TestData\\SceneIOTest\\myReadScene.json"), s);

		// Setup input arguments
		Path dir = Paths.get("Scene\\src\\Data\\TestData\\SceneIOTest\\");

		// read file
		IScene scene = SceneIO.read(s, dir);

		// verify file content
		assertEquals(Math.PI/4.0, scene.getFovY(), 1e-5);

		assertEquals(
			screen,
			scene.getSurface().getUnfoldablePolyhedron()
		);

		IColouredPolyhedron[] models = scene.getModels();
		assertEquals(2, models.length);

		Matrix4d translate = new Matrix4d();
		translate.setIdentity();
		translate.setTranslation(new Vector3d(1,2,3));
		model1.transform(translate);
		assert(models[0].equals(model1));

		assert(models[1].equals(model2));
	}

	@Test
	void write() throws IOException {

		// Setup scene objects

		double fovY = Math.PI/4.0;

		IUnfoldablePolyhedron screen = PolyhedronBuilder.getSphere(24, 8, true);

		IUnfoldablePolyhedron model1 = PolyhedronBuilder.getPyramid(false);
		Matrix4d translate = new Matrix4d();
		translate.setIdentity();
		translate.setTranslation(new Vector3d(1,2,3));
		model1.transform(translate);

		IUnfoldablePolyhedron model2 = PolyhedronBuilder.getCube(false);
		model2 = new UnfoldablePolyhedron(
			model2.getVertices(),
			model2.getEdgeIndices(),
			model2.getFaceIndices(),
			new Color[] {Color.red, Color.orange, Color.yellow, Color.green, Color.blue, Color.magenta},
			null
		);

		// create scene object

		IScene scene = new Scene(
			new Surface(screen),
			new IColouredPolyhedron[] {model1, model2},
			fovY
		);

		// write
		Path dir = Paths.get("Scene\\src\\Data\\TestData\\SceneIOTest\\");
		Path scenePath = Paths.get("Scene\\src\\Data\\TestData\\SceneIOTest\\scene.json");
		Path screenPath = Paths.get("Scene\\src\\Data\\TestData\\SceneIOTest\\screen.obj");
		Path model1Path = Paths.get("Scene\\src\\Data\\TestData\\SceneIOTest\\model0000.obj");
		Path model2Path = Paths.get("Scene\\src\\Data\\TestData\\SceneIOTest\\model0001.obj");

		// write file
		SceneIO.write(scene, dir);

		// verify file content

		String s = "{\n" +
			"  \"view\": {\n" +
			"    \"fov\": 45.0\n" +
			"  },\n" +
			"  \"models\": [\n" +
			"    {\n" +
			"      \"file\": \"model0000.obj\",\n" +
			"      \"transformations\": []\n" +
			"    },\n" +
			"    {\n" +
			"      \"file\": \"model0001.obj\",\n" +
			"      \"transformations\": []\n" +
			"    }\n" +
			"  ],\n" +
			"  \"screen\": {\n" +
			"    \"file\": \"screen.obj\",\n" +
			"    \"transformations\": []\n" +
			"  }\n" +
			"}";

		assertEquals(s, TextIO.read(scenePath));

		assertEquals(
			screen,
			PolyhedronIO.read(TextIO.read(screenPath))
		);

		assertEquals(
			model1,
			PolyhedronIO.read(TextIO.read(model1Path))
		);

		assertEquals(
			model2,
			PolyhedronIO.read(TextIO.read(model2Path))
		);
	}

	@Test
	void forCoverageSake() {
		new SceneIO();
	}
}