package SceneIO;

import TextIO.TextIO;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class GsonSceneIOTest {

	@Test
	void read() throws IOException {

		// prepare file to read
		String s = "{\n" +
			"  \"view\": {\n" +
			"    \"fov\": 40.0\n" +
			"  },\n" +
			"  \"models\": [\n" +
			"    {\n" +
			"      \"file\": \"a.txt\",\n" +
			"      \"transformations\": [\n" +
			"        \"translate 0 0 0\",\n" +
			"        \"scale 1\"\n" +
			"    ]" +
			"    },\n" +
			"    {\n" +
			"      \"file\": \"b.txt\",\n" +
			"      \"transformations\": []\n" +
			"    },\n" +
			"    {\n" +
			"      \"file\": \"c.txt\",\n" +
			"      \"transformations\": []\n" +
			"    }\n" +
			"  ],\n" +
			"  \"screen\": {\n" +
			"    \"file\": \"d.txt\"\n" +
			"  }\n" +
			"}";
		Path path = Paths.get("Scene\\src\\Data\\TestData\\GsonSceneIOTest\\readTest.txt");
		TextIO.write(path, s);

		// read file
		GsonScene gsonScene = GsonSceneIO.read(TextIO.read(path));

		assertEquals(40, gsonScene.view.fov);

		assertEquals(3, gsonScene.models.length);
		assertEquals("a.txt", gsonScene.models[0].file);
		assertArrayEquals(new String[] {"translate 0 0 0", "scale 1"}, gsonScene.models[0].transformations);
		assertEquals("b.txt", gsonScene.models[1].file);
		assertArrayEquals(new String[0], gsonScene.models[1].transformations);
		assertEquals("c.txt", gsonScene.models[2].file);
		assertArrayEquals(new String[0], gsonScene.models[2].transformations);

		assertEquals("d.txt", gsonScene.screen.file);
		assertArrayEquals(new String[0], gsonScene.screen.transformations);
	}

	@Test
	void write() throws IOException {

		// prepare gsonScene to write
		View view = new View();
		view.fov = 40;

		Model model1 = new Model();
		model1.file = "a.txt";
		Model model2 = new Model();
		model2.file = "b.txt";
		Model model3 = new Model();
		model3.file = "c.txt";

		Screen screen = new Screen();
		screen.file = "d.txt";

		GsonScene gsonScene = new GsonScene();
		gsonScene.view = view;
		gsonScene.models = new Model[] {model1, model2, model3};
		gsonScene.screen = screen;

		// write file
		Path path = Paths.get("Scene\\src\\Data\\TestData\\GsonSceneIOTest\\writeTest.txt");
		TextIO.write(path, GsonSceneIO.write(gsonScene));
		String actual = TextIO.read(path);

		String expected = "{\n" +
			"  \"view\": {\n" +
			"    \"fov\": 40.0\n" +
			"  },\n" +
			"  \"models\": [\n" +
			"    {\n" +
			"      \"file\": \"a.txt\",\n" +
			"      \"transformations\": []\n" +
			"    },\n" +
			"    {\n" +
			"      \"file\": \"b.txt\",\n" +
			"      \"transformations\": []\n" +
			"    },\n" +
			"    {\n" +
			"      \"file\": \"c.txt\",\n" +
			"      \"transformations\": []\n" +
			"    }\n" +
			"  ],\n" +
			"  \"screen\": {\n" +
			"    \"file\": \"d.txt\",\n" +
			"    \"transformations\": []\n" +
			"  }\n" +
			"}";

		assertEquals(expected, actual);
	}

	@Test
	void forCoverageSake() {
		new GsonSceneIO();
	}

}