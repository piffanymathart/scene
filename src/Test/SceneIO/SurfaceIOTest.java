package SceneIO;

import PolyhedronBuilder.PolyhedronBuilder;
import PolyhedronIO.PolyhedronIO;
import Scene.ISurface;
import TextIO.TextIO;
import Unfolding.IUnfoldablePolyhedron;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class SurfaceIOTest {

	@Test
	void read_fromFile() throws IOException {

		// setup obj file content
		IUnfoldablePolyhedron cube = PolyhedronBuilder.getCube(true);
		String s = PolyhedronIO.write(cube);
		TextIO.write(Paths.get("Scene\\src\\Data\\TestData\\SurfaceIOTest\\testScreen.obj"), s);

		// setup input parameters
		Screen screen = new Screen();
		screen.file = "testScreen.obj";
		Path dir = Paths.get("Scene\\src\\Data\\TestData\\SurfaceIOTest\\");

		// read from file
		ISurface surface = SurfaceIO.read(screen, dir);

		// verify content read is correct
		assertEquals(surface.getUnfoldablePolyhedron(), cube);
	}

	@Test
	void read_fromFile_invalidExtension() throws IOException {

		// setup obj file content
		IUnfoldablePolyhedron cube = PolyhedronBuilder.getCube(true);
		String s = PolyhedronIO.write(cube);
		TextIO.write(Paths.get("Scene\\src\\Data\\TestData\\SurfaceIOTest\\testScreen.gif"), s);

		// setup input parameters
		Screen screen = new Screen();
		screen.file = "testScreen.gif";
		Path dir = Paths.get("Scene\\src\\Data\\TestData\\SurfaceIOTest\\");

		// read from file
		assertThrows(IOException.class, () -> SurfaceIO.read(screen, dir));
	}

	@Test
	void read_fromFile_fileNotFound() {

		// setup input parameters
		Screen screen = new Screen();
		screen.file = "asdfasdf.obj";
		Path dir = Paths.get("Scene\\src\\Data\\TestData\\SurfaceIOTest\\");

		// read from file
		assertThrows(IOException.class, () -> SurfaceIO.read(screen, dir));
	}

	@Test
	void read_fromPresets_sphere() throws IOException {

		// setup input parameters
		Screen screen = new Screen();
		screen.shape = "sphere";
		Path dir = Paths.get("dir\\doesnt\\matter\\for\\presets");

		// read from file
		ISurface surface = SurfaceIO.read(screen, dir);

		// verify content read is correct
		assertEquals(
			surface.getUnfoldablePolyhedron(),
			PolyhedronBuilder.getSphere(36, 12, true));
	}

	@Test
	void read_fromPresets_hemisphere() throws IOException {

		// setup input parameters
		Screen screen = new Screen();
		screen.shape = "hemisphere";
		Path dir = Paths.get("dir\\doesnt\\matter\\for\\presets");

		// read from file
		ISurface surface = SurfaceIO.read(screen, dir);

		// verify content read is correct
		assertEquals(
			surface.getUnfoldablePolyhedron(),
			PolyhedronBuilder.getHemisphere(36, 12, true));
	}

	@Test
	void read_fromPresets_cube() throws IOException {

		// setup input parameters
		Screen screen = new Screen();
		screen.shape = "cube";
		Path dir = Paths.get("dir\\doesnt\\matter\\for\\presets");

		// read from file
		ISurface surface = SurfaceIO.read(screen, dir);

		// verify content read is correct
		assertEquals(
			surface.getUnfoldablePolyhedron(),
			PolyhedronBuilder.getCube(true));
	}

	@Test
	void read_fromPresets_pyramid() throws IOException {

		// setup input parameters
		Screen screen = new Screen();
		screen.shape = "pyramid";
		Path dir = Paths.get("dir\\doesnt\\matter\\for\\presets");

		// read from file
		ISurface surface = SurfaceIO.read(screen, dir);

		// verify content read is correct
		assertEquals(
			surface.getUnfoldablePolyhedron(),
			PolyhedronBuilder.getPyramid(true));
	}

	@Test
	void read_fromPresets_invalid() {

		// setup input parameters
		Screen screen = new Screen();
		screen.shape = "pentagonalBipyramid";
		Path dir = Paths.get("dir\\doesnt\\matter\\for\\presets");

		// read from file
		assertThrows(IOException.class, () -> SurfaceIO.read(screen, dir));
	}

	@Test
	void read_emptyScreen() {

		Path dir = Paths.get("dir\\doesnt\\matter\\for\\presets");

		assertThrows(IOException.class, () -> SurfaceIO.read(null, dir));
		assertThrows(IOException.class, () -> SurfaceIO.read(new Screen(), dir));
	}

	@Test
	void forCoverageSake() {
		new SurfaceIO();
	}
}