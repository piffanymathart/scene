package Scene;

import Mesh.IMesh;
import Polyhedron.ColouredPolyhedron;
import Polyhedron.IColouredPolyhedron;
import PolyhedronBuilder.PolyhedronBuilder;
import Unfolding.IUnfoldablePolyhedron;
import org.junit.jupiter.api.Test;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class SceneTest {

	@Test
	void valuesConstructor_getters() {

		IScene scene = createScene();

		assertEquals(
			scene.getSurface(),
			new Surface(PolyhedronBuilder.getCube(true))
		);

		assertArrayEquals(
			scene.getModels(),
			new IColouredPolyhedron[] {
				PolyhedronBuilder.getPyramid(false),
				PolyhedronBuilder.getCube(false)
			}
		);
		assertEquals(scene.getModel(0), PolyhedronBuilder.getPyramid(false));
		assertEquals(scene.getModel(1), PolyhedronBuilder.getCube(false));

		assertEquals(Math.PI/4.0, scene.getFovY());
		assertEquals(new Point3d(0,0,0), scene.getScreenFrontPos());
		assertEquals(new Vector3d(0,1,0), scene.getScreenUpVec());
	}

	@Test
	void setters() {

		IScene scene = createScene();

		scene.setModel(1, PolyhedronBuilder.getSphere(8, 4, false));

		assertArrayEquals(
			scene.getModels(),
			new IColouredPolyhedron[] {
				PolyhedronBuilder.getPyramid(false),
				PolyhedronBuilder.getSphere(8, 4, false)
			}
		);
		assertEquals(scene.getModel(0), PolyhedronBuilder.getPyramid(false));
		assertEquals(scene.getModel(1), PolyhedronBuilder.getSphere(8, 4, false));

		scene.setFovY(Math.PI/3.0);
		assertEquals(Math.PI/3.0, scene.getFovY());

		scene.setScreenFrontPos(new Point3d(1,2,3));
		assertEquals(new Point3d(1,2,3), scene.getScreenFrontPos());

		scene.setScreenUpVec(new Vector3d(4,5,6));
		assertEquals(new Vector3d(4,5,6), scene.getScreenUpVec());
	}

	@Test
	void recomputeProjection() {

		IScene scene = createScene();
		IMesh[][] projections = scene.getProjections();

		scene.setModel(0, PolyhedronBuilder.getCube(false));
		scene.setModel(1, PolyhedronBuilder.getPyramid(false));
		scene.recomputeProjection();
		IMesh[][] reprojections = scene.getProjections();

		assertEquals(projections.length, reprojections.length);

		for(int i=0; i<projections.length; i++) {
			assertEquals(2, projections[i].length);
			assertArrayEquals(
				new IMesh[] {projections[i][1], projections[i][0]},
				reprojections[i]
			);
		}
	}

	@Test
	void transform() {

		Matrix4d mat = new Matrix4d();
		mat.setIdentity();
		mat.setTranslation(new Vector3d(0.1, 0.2, 0.3));
		mat.rotX(40);

		IScene scene = createScene();
		scene.transform(mat);

		ISurface transSurface = new Surface(PolyhedronBuilder.getCube(true));
		transSurface.transform(mat);
		assertEquals(
			scene.getSurface(),
			transSurface
		);

		IColouredPolyhedron transModel1 = PolyhedronBuilder.getPyramid(false);
		transModel1.transform(mat);

		IColouredPolyhedron transModel2 = PolyhedronBuilder.getCube(false);
		transModel2.transform(mat);

		assertArrayEquals(
			scene.getModels(),
			new IColouredPolyhedron[] {transModel1, transModel2}
		);
		assertEquals(scene.getModel(0), transModel1);
		assertEquals(scene.getModel(1), transModel2);

		assertEquals(Math.PI/4.0, scene.getFovY());

		Point3d transFrontPos = new Point3d(0,0,0);
		mat.transform(transFrontPos);
		assertEquals(transFrontPos, scene.getScreenFrontPos());

		Vector3d transUpVec = new Vector3d(0,1,0);
		mat.transform(transUpVec);
		assertEquals(transUpVec, scene.getScreenUpVec());
	}

	@Test
	void getScreenNet() {

		IUnfoldablePolyhedron cube = PolyhedronBuilder.getCube(true);
		ISurface surface = new Surface(cube);

		IColouredPolyhedron triModel = new ColouredPolyhedron(
			new Point3d[]{
				new Point3d(0, 0, 0),
				new Point3d(1, 0, 0),
				new Point3d(0, 1, 0)
			},
			new int[0][0],
			new int[][] {{0,1,2}},
			null
		);

		IColouredPolyhedron[] models = { triModel };
		IScene scene = new Scene(surface, models, Math.PI/4.0);

		int w = 100;
		int h = 50;

		assertArrayEquals(cube.getUnfolding(w, h), scene.getScreenNet(w, h));
	}

	@Test
	void getProjectionNet() {

		IUnfoldablePolyhedron cube = PolyhedronBuilder.getCube(true);
		ISurface surface = new Surface(cube);

		IColouredPolyhedron triModel = new ColouredPolyhedron(
			new Point3d[]{
				new Point3d(0, 0, 0),
				new Point3d(1, 0, 0),
				new Point3d(0, 1, 0)
			},
			new int[0][0],
			new int[][] {{0,1,2}},
			null
		);

		IColouredPolyhedron sqrModel = new ColouredPolyhedron(
			new Point3d[]{
				new Point3d(0,0,2),
				new Point3d(2,0,2),
				new Point3d(2,2,2),
				new Point3d(0,2,2)
			},
			new int[0][0],
			new int[][] {{0,1,2,3}},
			null
		);

		IColouredPolyhedron[] models = { triModel, sqrModel };

		int w = 100;
		int h = 50;

		IScene scene = new Scene(surface, models, Math.PI/4.0);
		IMesh projectionNet = scene.getProjectionNet(w, h);

		IMesh expected = surface.getProjectionNet(scene.getProjections(), w, h);

		assertEquals(expected, projectionNet);
	}

	@Test
	void forCoverageSake() {
		new Scene();
	}

	private IScene createScene() {

		IUnfoldablePolyhedron cube = PolyhedronBuilder.getCube(true);
		ISurface surface = new Surface(cube);

		IColouredPolyhedron[] models = {
			PolyhedronBuilder.getPyramid(false),
			PolyhedronBuilder.getCube(false)
		};

		double fovY = Math.PI/4.0;

		return new Scene(surface, models, fovY);
	}
}