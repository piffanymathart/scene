package Scene;

import FlaggedProjection.Cell;
import FlaggedProjection.ICell;
import FlaggedProjection.Plane;
import Geometry.Lines.ILine3d;
import Geometry.Polygons.IPolygon3d;
import Geometry.Polygons.Polygon3d;
import Mesh.IMesh;
import Mesh.Mesh;
import PolyhedronBuilder.PolyhedronBuilder;
import Transformer.ITransformer;
import Transformer.Transformer;
import Unfolding.IUnfoldablePolyhedron;
import Unfolding.UnfoldablePolyhedron;
import org.junit.jupiter.api.Test;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class SurfaceTest {

	@Test
	void copyConstructor() {

		ISurface surface = new Surface(PolyhedronBuilder.getCube(true));
		ISurface copy = new Surface(surface);
		assertEquals(surface.getUnfoldablePolyhedron(), copy.getUnfoldablePolyhedron());
	}

	@Test
	void getUnfoldablePolyhedron() {

		IUnfoldablePolyhedron cube = PolyhedronBuilder.getCube(true);
		ISurface surface = new Surface(cube);
		assertEquals(cube, surface.getUnfoldablePolyhedron());
	}

	@Test
	void hasUnfolding() {

		for(boolean unfoldable : new boolean[] {true, false}) {
			IUnfoldablePolyhedron cube = PolyhedronBuilder.getCube(unfoldable);
			ISurface surface = new Surface(cube);
			assertEquals(unfoldable, surface.hasUnfolding());
		}
	}

	@Test
	void getFaces() {

		IUnfoldablePolyhedron cube = PolyhedronBuilder.getCube(true);
		ISurface surface = new Surface(cube);
		assertArrayEquals(cube.getFaces(), surface.getFaces());
	}

	@Test
	void getNumFaces() {

		IUnfoldablePolyhedron cube = PolyhedronBuilder.getCube(true);
		ISurface surface = new Surface(cube);
		assertEquals(6, surface.getNumFaces());
	}

	@Test
	void getFace() {

		IUnfoldablePolyhedron cube = PolyhedronBuilder.getCube(true);
		ISurface surface = new Surface(cube);
		for(int i=0; i<6; i++) {
			assertEquals(cube.getFace(i), surface.getFace(i));
		}
	}

	@Test
	void getCells_getNumCells_singleFace() {

		for(int i=0; i<7; i++) {
			Map.Entry<IPolygon3d, ICell[]> pair = createFaceCellPair(i);
			IPolygon3d face = pair.getKey();
			ICell[] cells = pair.getValue();
			IUnfoldablePolyhedron polyhedron = createPolyhedronWithFace(face);
			ISurface surface = new Surface(polyhedron);

			assertArrayEquals(cells, surface.getCells());
			assertEquals(cells.length, surface.getNumCells());
		}
	}

	@Test
	void getCells_getNumCells_multipleFaces() {

		IPolygon3d[] faces = new IPolygon3d[7];
		List<ICell> cells = new ArrayList<>();
		for(int i=0; i<7; i++) {
			Map.Entry<IPolygon3d, ICell[]> pair = createFaceCellPair(i);
			faces[i] = pair.getKey();
			cells.addAll(Arrays.asList(pair.getValue()));
		}

		Point3d[] verts = {
			faces[6].get(0), faces[6].get(1), faces[6].get(2),
			faces[6].get(3), faces[6].get(4), faces[6].get(5)
		};
		int[][] edgeIndices = {};
		int[][] faceIndices = {
			{},
			{0},
			{0,1},
			{0,1,2},
			{0,1,2,3},
			{0,1,2,3,4},
			{0,1,2,3,4,5}
		};

		IUnfoldablePolyhedron polyhedron = new UnfoldablePolyhedron(
			verts,
			edgeIndices,
			faceIndices,
			null,
			null
		);
		ISurface surface = new Surface(polyhedron);

		assertArrayEquals(cells.toArray(new ICell[cells.size()]), surface.getCells());
		assertEquals(cells.size(), surface.getNumCells());
	}

	@Test
	void getCentre() {

		Point3d p1 = new Point3d(0,0,0);
		Point3d p2 = new Point3d(1,0,0);
		Point3d p3 = new Point3d(2,1,0);
		Point3d p4 = new Point3d(2,2,0);
		Point3d p5 = new Point3d(1,2,0);
		Point3d p6 = new Point3d(0,1,0);

		Point3d[] verts = {p1,p2,p3,p4,p5,p6};
		int[][] edgeIndices = {{5,0},{0,1}};
		int[][] faceIndices = {{2,1,0},{2,1,5}};

		IUnfoldablePolyhedron polyhedron = new UnfoldablePolyhedron(
			verts,
			edgeIndices,
			faceIndices,
			null,
			null
		);
		ISurface surface = new Surface(polyhedron);

		assertEquals(new Point3d(1,0.5,0), surface.getCentre());
	}

	@Test
	void transform() {

		Point3d p1 = new Point3d(0,0,0);
		Point3d p2 = new Point3d(1,0,0);
		Point3d p3 = new Point3d(2,1,0);
		Point3d p4 = new Point3d(2,2,0);
		Point3d p5 = new Point3d(1,2,0);
		Point3d p6 = new Point3d(0,1,0);

		Point3d q1 = new Point3d(0.1,0.2,0.3);
		Point3d q2 = new Point3d(1.1,0.2,0.3);
		Point3d q3 = new Point3d(2.1,1.2,0.3);
		Point3d q4 = new Point3d(2.1,2.2,0.3);
		Point3d q5 = new Point3d(1.1,2.2,0.3);
		Point3d q6 = new Point3d(0.1,1.2,0.3);

		Point3d[] verts1 = {p1,p2,p3,p4,p5,p6};
		int[][] edgeIndices = {{5,0},{0,1}};
		int[][] faceIndices = {{2,1,0},{2,1,5}};

		IUnfoldablePolyhedron polyhedron1 = new UnfoldablePolyhedron(
			verts1,
			edgeIndices,
			faceIndices,
			null,
			null
		);
		ISurface actual = new Surface(polyhedron1);

		Matrix4d transMat = new Matrix4d();
		transMat.setIdentity();
		transMat.setTranslation(new Vector3d(0.1, 0.2, 0.3));
		actual.transform(transMat);

		Point3d[] verts2 = {q1,q2,q3,q4,q5,q6};
		IUnfoldablePolyhedron polyhedron2 = new UnfoldablePolyhedron(
			verts2,
			edgeIndices,
			faceIndices,
			null,
			null
		);
		ISurface expected = new Surface(polyhedron2);

		assertEquals(expected.getUnfoldablePolyhedron(), actual.getUnfoldablePolyhedron());
		assertEquals(expected.hasUnfolding(), actual.hasUnfolding());
		assertArrayEquals(expected.getFaces(), actual.getFaces());
		assertArrayEquals(expected.getCells(), actual.getCells());
		assertEquals(expected.getCentre(), actual.getCentre());
	}

	@Test
	void getProjectionNet() {

		IUnfoldablePolyhedron cube = PolyhedronBuilder.getCube(true);
		ISurface surface = new Surface(cube);

		IPolygon3d triPoly = new Polygon3d(
			new Point3d(0,0,0),
			new Point3d(1,0,0),
			new Point3d(0,1,0)
		);

		IPolygon3d sqrPoly = new Polygon3d(
			new Point3d(0,0,2),
			new Point3d(2,0,2),
			new Point3d(2,2,2),
			new Point3d(0,2,2)
		);

		IMesh triMesh = new Mesh(
			new ILine3d[0],
			new IPolygon3d[] {triPoly}
		);

		IMesh sqrMesh = new Mesh(
			new ILine3d[0],
			new IPolygon3d[] {sqrPoly}
		);

		// corresponds to 6 sets of projections on 6 cells (3 faces)
		IMesh[][] projections = {
			{new Mesh(triMesh), new Mesh(sqrMesh)}, // cell 1 of face 1
			{new Mesh(triMesh), new Mesh(sqrMesh)}, // cell 2 of face 1
			{new Mesh(triMesh), new Mesh(sqrMesh)}, // cell 1 of face 2
			{new Mesh(triMesh), new Mesh(sqrMesh)}, // cell 2 of face 2
			{new Mesh(triMesh), new Mesh(sqrMesh)}, // cell 1 of face 3
			{new Mesh(triMesh), new Mesh(sqrMesh)}, // cell 2 of face 3
			{new Mesh(triMesh), new Mesh(sqrMesh)}, // cell 1 of face 4
			{new Mesh(triMesh), new Mesh(sqrMesh)}, // cell 2 of face 4
			{new Mesh(triMesh), new Mesh(sqrMesh)}, // cell 1 of face 5
			{new Mesh(triMesh), new Mesh(sqrMesh)}, // cell 2 of face 5
			{new Mesh(triMesh), new Mesh(sqrMesh)}, // cell 1 of face 6
			{new Mesh(triMesh), new Mesh(sqrMesh)}  // cell 2 of face 6
		};

		int w = 100;
		int h = 50;

		ITransformer transformer = new Transformer();

		Matrix4d[] unfoldingTransforms = cube.getUnfoldingTransforms(w, h);
		IPolygon3d[] netFaces = surface.getProjectionNet(projections, w, h).getFaces();

		for(int i=0; i<12; i++) {
			for(int j=0; j<2; j++) {
				IPolygon3d expected = (j==0) ? triPoly : sqrPoly;
				int faceInd = i/2;
				expected = new Polygon3d(
					transformer.transform(
						unfoldingTransforms[faceInd],
						expected.getVertices()
					)
				);
				assertAlmostEqual(expected, netFaces[2*i+j]);
			}
		}

		Matrix4d mat = new Matrix4d();
		mat.setIdentity();
		mat.rotX(10);
		mat.rotY(20);

		surface.transform(mat);

		for(int i=0; i<12; i++) {
			for (int j = 0; j < 2; j++) {
				projections[i][j].transform(mat);
			}
		}

		unfoldingTransforms = cube.getUnfoldingTransforms(w, h);
		netFaces = surface.getProjectionNet(projections, w, h).getFaces();

		for(int i=0; i<12; i++) {
			for(int j=0; j<2; j++) {
				IPolygon3d expected = (j==0) ? triPoly : sqrPoly;
				int faceInd = i/2;
				expected = new Polygon3d(
					transformer.transform(
						unfoldingTransforms[faceInd],
						expected.getVertices()
					)
				);
				assertAlmostEqual(expected, netFaces[2*i+j]);
			}
		}
	}

	@Test
	void equals() {

		ISurface surface1 = new Surface(PolyhedronBuilder.getCube(true));
		ISurface surface2 = new Surface(PolyhedronBuilder.getCube(false));

		assertEquals(surface1, new Surface(surface1));
		assertNotEquals(surface1, surface2);
	}

	private void assertAlmostEqual(IPolygon3d polygon1, IPolygon3d polygon2) {

		assertEquals(polygon1.size(), polygon2.size());
		for(int i=0; i<polygon1.size(); i++) {
			assert(polygon1.get(i).distance(polygon2.get(i)) <= 1e-5);
		}
	}

	private IUnfoldablePolyhedron createPolyhedronWithFace(IPolygon3d face) {

		Point3d[] verts = face.getVertices();
		int n = verts.length;

		int[][] edgeIndices = {};

		int[] faceIndex = new int[n];
		for(int i=0; i<n; i++) faceIndex[i] = i;
		int[][] faceIndices = {faceIndex};

		return new UnfoldablePolyhedron(
			verts,
			edgeIndices,
			faceIndices,
			null,
			null
		);
	}

	private Map.Entry<IPolygon3d, ICell[]> createFaceCellPair(int n) {

		assert(n<=6);

		Point3d p1 = new Point3d(0,0,0);
		Point3d p2 = new Point3d(1,0,0);
		Point3d p3 = new Point3d(2,1,0);
		Point3d p4 = new Point3d(2,2,0);
		Point3d p5 = new Point3d(1,2,0);
		Point3d p6 = new Point3d(0,1,0);

		Point3d[] allVerts = {p1,p2,p3,p4,p5,p6};
		Point3d[] verts = new Point3d[n];
		for(int i=0; i<n; i++) verts[i] = allVerts[i];

		IPolygon3d face = new Polygon3d(verts);

		ICell cell1 = new Cell(new Plane(p1,p2,p3));
		ICell cell2 = new Cell(new Plane(p1,p3,p4));
		ICell cell3 = new Cell(new Plane(p1,p4,p5));
		ICell cell4 = new Cell(new Plane(p1,p5,p6));

		ICell[] cells = new ICell[0];
		switch(n) {
			case 3: cells = new ICell[] {cell1}; break;
			case 4: cells = new ICell[] {cell1, cell2}; break;
			case 5: cells = new ICell[] {cell1, cell2, cell3}; break;
			case 6: cells = new ICell[] {cell1, cell2, cell3, cell4}; break;
		}

		return new AbstractMap.SimpleEntry<>(face, cells);
	}

}