package SceneIO;

/**
 * Model structure for read/write via Gson reflection.
 */
public class Model {

	public String file;
	public String shape;
	public String[] transformations = {};
}
