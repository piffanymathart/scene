package SceneIO;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


class GsonSceneIO {

	/**
	 * Reads a GSON scene from a string.
	 * @param s The string.
	 */
	static GsonScene read(String s) {
		Gson gson = new Gson();
		return gson.fromJson(s, GsonScene.class);
	}

	/**
	 * Writes a GSON scene to a string.
	 * @param gsonScene The GSON Scene.
	 */
	static String write(GsonScene gsonScene) {
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		return gson.toJson(gsonScene);
	}
}
