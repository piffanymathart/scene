package SceneIO;

import PolyhedronBuilder.PolyhedronBuilder;
import PolyhedronIO.PolyhedronIO;
import Scene.ISurface;
import Scene.Surface;
import TextIO.TextIO;
import Unfolding.IUnfoldablePolyhedron;

import javax.vecmath.Matrix4d;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

class SurfaceIO {

	static ISurface read(Screen screen, Path dir) throws IOException {

		if(screen==null) throw new IOException();

		IUnfoldablePolyhedron polyhedron;
		if(screen.file != null) polyhedron = readFromFile(screen.file, dir);
		else if(screen.shape != null) polyhedron = readFromPresets(screen.shape);
		else throw new IOException();

		Matrix4d screenTransform = TransformIO.read(screen.transformations);
		polyhedron.transform(screenTransform);

		return new Surface(polyhedron);
	}

	private static IUnfoldablePolyhedron readFromFile(String file, Path dir) throws IOException {
		Path scenePath = appendToPath(dir, file);
		if(hasExtension(scenePath,".obj")) return PolyhedronIO.read(TextIO.read(scenePath));
		else throw new IOException();
	}

	private static IUnfoldablePolyhedron readFromPresets(String shape) throws IOException {
		switch (shape) {
			case "sphere":
				return PolyhedronBuilder.getSphere(36, 12, true);
			case "hemisphere":
				return PolyhedronBuilder.getHemisphere(36, 12, true);
			case "cube":
				return PolyhedronBuilder.getCube(true);
			case "pyramid":
				return PolyhedronBuilder.getPyramid(true);
		}
		throw new IOException();
	}

	private static boolean hasExtension(Path path, String extension) {
		return path.toString().toLowerCase().endsWith(extension);
	}

	private static Path appendToPath(Path dirPath, String filename) {
		return Paths.get(dirPath.toString() + "\\" + filename);
	}
}
