package SceneIO;

import Polyhedron.IColouredPolyhedron;
import PolyhedronIO.PolyhedronIO;
import Scene.IScene;
import Scene.ISurface;
import Scene.Scene;
import TextIO.TextIO;
import Unfolding.UnfoldablePolyhedron;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Performs IO operations for projection scenes.
 */
public class SceneIO {

	/**
	 * Reads a scene from a file string and a file directory path.
	 * @param s The file string.
	 * @param dir The file directory path.
	 * @return The projection scene.
	 */
	public static IScene read(String s, Path dir) throws IOException {

		GsonScene gsonScene = GsonSceneIO.read(s);

		ISurface surface = SurfaceIO.read(gsonScene.screen, dir);
		IColouredPolyhedron[] models = ModelsIO.read(gsonScene.models, dir);

		return new Scene(
			surface,
			models,
			ViewIO.readFovY(gsonScene.view)
		);
	}

	/**
	 * Writes a projection scene to the file directory path.
	 * @param scene The projection scene.
	 * @param dir The file directory path.
	 */
	public static void write(IScene scene, Path dir) throws IOException {

		GsonScene gsonScene = new GsonScene();

		gsonScene.view = new View();
		gsonScene.view.fov = scene.getFovY()/Math.PI*180.0;

		int n = scene.getModels().length;
		gsonScene.models = new Model[n];
		for(int i=0; i<n; i++) {
			gsonScene.models[i] = new Model();
			gsonScene.models[i].file = String.format("model%04d.obj", i);
		}

		gsonScene.screen = new Screen();
		gsonScene.screen.file = "screen.obj";

		String sceneStr = GsonSceneIO.write(gsonScene);
		TextIO.write(appendToPath(dir, "scene.json"), sceneStr);

		String screenStr = PolyhedronIO.write(scene.getSurface().getUnfoldablePolyhedron());
		TextIO.write(appendToPath(dir, gsonScene.screen.file), screenStr);

		for(int i=0; i<n; i++) {
			String modelStr = PolyhedronIO.write(new UnfoldablePolyhedron(scene.getModel(i), null));
			TextIO.write(appendToPath(dir, gsonScene.models[i].file), modelStr);
		}
	}

	private static Path appendToPath(Path dirPath, String filename) {
		return Paths.get(dirPath.toString() + "\\" + filename);
	}
}
