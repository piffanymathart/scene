package SceneIO;

import Rotater.Rotater;

import javax.vecmath.Matrix3d;
import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.io.IOException;

/**
 * Reads affine transformations and combines them into a single matrix.
 */
class TransformIO {

	/**
	 * Reads affine transformations and combines them into a single matrix.
	 * @param ss The strings describing the individual transformations.
	 * @return The matrix that combines all the transformations.
	 * @throws IOException If the input strings are invalid transformations.
	 */
	static Matrix4d read(String[] ss) throws IOException {

		double scale = 1;
		Matrix4d rotTransMat = new Matrix4d();
		rotTransMat.setIdentity();

		for(String s : ss) {
			if (s.startsWith("scale")) {
				try {
					scale *= parseScale(s);
				}
				catch(IOException e) {
					throw new IOException("Invalid scaling; proper format is \"scale {val}\"");
				}
			} else if (s.startsWith("translate")) {
				try {
					rotTransMat.mul(parseTranslation(s), rotTransMat);
				}
				catch(IOException e) {
					throw new IOException("Invalid transformation; proper format is \"translate {x} {y} {z}\"");
				}
			} else if (s.startsWith("rotate")) {
				try {
					rotTransMat.mul(parseRotation(s), rotTransMat);
				}
				catch(IOException e) {
					throw new IOException("Invalid rotation; proper format is \"rotate about {axis.x} {axis.y} {axis.z} by {degrees}\"");
				}
			} else {
				throw new IOException(
					"Unknown transformation type; should be one of scale, translate, or rotate."
				);
			}
		}

		Matrix3d id = new Matrix3d();
		id.setIdentity();

		Matrix4d m = new Matrix4d(rotTransMat);
		m.setScale(scale);

		return m;
	}

	/**
	 * Parses a rotation string.
	 * @param s0 The rotation string.
	 * @return The corresponding affine transformation matrix.
	 * @throws IOException If the input string is invalid.
	 */
	private static Matrix4d parseRotation(String s0) throws IOException {
		String s = s0;
		s = s.replaceAll("rotate about ", "");
		s = s.trim();
		String[] parts = s.split(" by ");
		if(parts.length != 2) throw new IOException();
		double[] axis = parseValues(parts[0]);
		if(axis.length != 3) throw new IOException();
		double[] angle = parseValues(parts[1]);
		if(angle.length != 1) throw new IOException();
		return new Rotater().createRotationMatrix(
			new Vector3d(axis),
			angle[0] / 180.0 * Math.PI,
			new Point3d(0, 0, 0)
		);
	}

	/**
	 * Parses a scaling string.
	 * @param s0 The scaling string.
	 * @return The corresponding affine transformation matrix.
	 * @throws IOException If the input string is invalid.
	 */
	private static double parseScale(String s0) throws IOException {
		String s = s0;
		s = s.replaceAll("scale ", "");
		s = s.trim();

		double[] vals = parseValues(s);
		if(vals.length != 1) throw new IOException();
		return vals[0];
	}

	/**
	 * Parses a translation string.
	 * @param s0 The translation string.
	 * @return The corresponding affine transformation matrix.
	 * @throws IOException If the input string is invalid.
	 */
	private static Matrix4d parseTranslation(String s0) throws IOException {
		String s = s0;
		s = s.replaceAll("translate ", "");
		s = s.trim();
		double[] vals = parseValues(s);
		if (vals.length == 3) {
			return new Matrix4d(
				1,0,0,vals[0],
				0,1,0,vals[1],
				0,0,1,vals[2],
				0,0,0,1
			);
		}
		throw new IOException();
	}

	private static double[] parseValues(String s) throws IOException {
		String[] strVals = s.split("\\s+");
		int n = strVals.length;
		double[] dVals = new double[n];
		for (int i = 0; i < n; i++) {
			try {
				dVals[i] = Double.valueOf(strVals[i]);
			} catch (NumberFormatException e) {
				throw new IOException();
			}
		}
		return dVals;
	}
}
