package SceneIO;

import java.io.IOException;


/**
 * Reads in view parameters.
 */
public class ViewIO {

	/**
	 * Reads the vertical field-of-view value.
	 * @param view The View object.
	 * @return The vertical field-of-view value.
	 */
	public static double readFovY(View view) throws IOException {

		if(view==null || view.fov==0) {
			throw new IOException(
				"Must specify a non-zero vertical field-of-view (fov) value in degrees."
			);
		}

		return view.fov/180.0*Math.PI;
	}

	/**
	 * Writes the vertical field-of-view value to a Gson View value.
	 * @param fovY The vertical field-of-view value.
	 * @return The corresponding Gson View value.
	 */
	public static double writeFovY(double fovY) throws IOException {

		return fovY/Math.PI*180.0;
	}
}
