package SceneIO;

/**
 * GsonScene structure for read/write via Gson reflection.
 */
class GsonScene {

	/**
	 * Gson view structure.
	 */
	View view;

	/**
	 * Gson model structures.
	 */
	Model[] models;

	/**
	 * Gson screen structure.
	 */
	Screen screen;

	/**
	 * Constructs an empty scene.
	 */
	GsonScene() {}
}
