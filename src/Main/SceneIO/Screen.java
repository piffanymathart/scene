package SceneIO;

/**
 * Screen structure for read/write via Gson reflection.
 */
public class Screen {

	public String file;
	public String shape;
	public String[] transformations = {};
}
