package SceneIO;

/**
 * View structure for read/write via GSON reflection.
 */
public class View {

	/**
	 * The vertical field-of-view.
	 */
	public double fov;
}
