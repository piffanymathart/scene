package SceneIO;

import Polyhedron.IColouredPolyhedron;
import PolyhedronBuilder.PolyhedronBuilder;
import PolyhedronIO.PolyhedronIO;
import TextIO.TextIO;

import javax.imageio.ImageIO;
import javax.vecmath.Matrix4d;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

class ModelsIO {

	static IColouredPolyhedron[] read(Model[] models, Path dir) throws IOException {

		if(models == null) {
			throw new IOException("Invalid models.");
		}

		int n = models.length;
		IColouredPolyhedron[] polyhedra = new IColouredPolyhedron[n];

		for(int i=0; i<n; i++) {

			Model model = models[i];

			IColouredPolyhedron modelPolyhedron;
			if(model.file != null) modelPolyhedron = readFromFile(model.file, dir);
			else if(model.shape != null) modelPolyhedron = readFromPresets(model.shape);
			else throw new IOException();

			Matrix4d modelTransform = TransformIO.read(model.transformations);
			modelPolyhedron.transform(modelTransform);

			polyhedra[i] = modelPolyhedron;
		}
		return polyhedra;
	}

	private static IColouredPolyhedron readFromFile(String file, Path dir) throws IOException {
		Path modelPath = appendToPath(dir,file);
		if(hasExtension(modelPath,".obj")) return PolyhedronIO.read(TextIO.read(modelPath));
		else if(hasExtension(modelPath, ".png")) {
			BufferedImage img = ImageIO.read(new File(modelPath.toString()));
			return PolyhedronIO.read(img);
		}
		else throw new IOException();
	}

	private static IColouredPolyhedron readFromPresets(String shape) throws IOException {
		switch (shape) {
			case "sphere":
				return PolyhedronBuilder.getSphere(36, 12, false);
			case "hemisphere":
				return PolyhedronBuilder.getHemisphere(36, 12, false);
			case "cube":
				return PolyhedronBuilder.getCube(false);
			case "pyramid":
				return PolyhedronBuilder.getPyramid(false);
		}
		throw new IOException();
	}

	private static boolean hasExtension(Path path, String extension) {
		return path.toString().toLowerCase().endsWith(extension);
	}

	private static Path appendToPath(Path dirPath, String filename) {
		return Paths.get(dirPath.toString() + "\\" + filename);
	}
}
