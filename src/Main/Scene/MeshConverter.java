package Scene;

import Geometry.Lines.ILine3d;
import Geometry.Polygons.IPolygon3d;
import Mesh.*;
import Polyhedron.IColouredPolyhedron;
import Polyhedron.IPolyhedronHelper;
import Polyhedron.PolyhedronHelper;

import javax.vecmath.Vector3d;
import java.awt.*;
import java.util.Arrays;
import java.util.List;

public class MeshConverter {

	private static IPolyhedronHelper POLYHEDRON_HELPER = new PolyhedronHelper();

	public static IMesh toMesh(IColouredPolyhedron polyhedron) {
		ILine3d[] edges = polyhedron.getEdges();
		IPolygon3d[] faces = polyhedron.getFaces();
		Vector3d[] edgeNormals1 = new Vector3d[edges.length];
		Vector3d[] edgeNormals2 = new Vector3d[edges.length];
		Vector3d[] faceNormals = new Vector3d[faces.length];
		POLYHEDRON_HELPER.getEdgeAndFaceNormals(polyhedron, edgeNormals1, edgeNormals2, faceNormals);
		IMeshPolygon[] meshFaces = createMeshFaces(faces, faceNormals, polyhedron.getFaceColours());
		IMeshLine[] meshEdges = createMeshEdges(edges, edgeNormals1, edgeNormals2);
		return new Mesh(meshEdges, meshFaces);
	}

	private static IMeshPolygon[] createMeshFaces(IPolygon3d[] faces, Vector3d[] faceNormals, Color[] faceColours) {
		int n = faces.length;
		IMeshPolygon[] meshFaces = new IMeshPolygon[n];
		for(int i=0; i<n; i++) {
			Color c = faceColours==null ? null : faceColours[i];
			meshFaces[i] = new MeshPolygon(faces[i].getVertices(), faceNormals[i], false, c);
		}
		return meshFaces;
	}

	private static IMeshLine[] createMeshEdges(ILine3d[] edges, Vector3d[] edgeNormals1, Vector3d[] edgeNormals2) {
		int n = edges.length;
		IMeshLine[] meshEdges = new IMeshLine[n];
		for(int i=0; i<n; i++) {
			List<Vector3d> normals = Arrays.asList(edgeNormals1[i], edgeNormals2[i]);
			meshEdges[i] = new MeshLine(edges[i].getP1(), edges[i].getP2(), normals, false);
		}
		return meshEdges;
	}

}
