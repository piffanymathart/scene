package Scene;

import FlaggedProjection.ICell;
import Geometry.Polygons.IPolygon3d;
import Mesh.IMesh;
import Unfolding.IUnfoldablePolyhedron;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;

/**
 * The projection surface containing an underlying polyhedron.
 */
public interface ISurface {

	/**
	 * Gets a copy of the underlying unfoldable polyhedron.
	 * @return The underlying unfoldable polyhedron.
	 */
	IUnfoldablePolyhedron getUnfoldablePolyhedron();

	/**
	 * Checks whether or not the surface has an unfolding.
	 * @return Whether or not the surface has an unfolding.
	 */
	boolean hasUnfolding();

	/**
	 * Gets the faces.
	 * @return The faces.
	 */
	IPolygon3d[] getFaces();

	/**
	 * Gets the number of faces.
	 * @return The number of faces.
	 */
	int getNumFaces();

	/**
	 * Gets the face specified by the index.
	 * @param index The face index.
	 * @return The face specified by the index.
	 */
	IPolygon3d getFace(int index);

	/**
	 * Gets the triangular projection cells.
	 * @return The triangular projection cells.
	 */
	ICell[] getCells();

	/**
	 * Gets the number of cells.
	 * @return The number of cells.
	 */
	int getNumCells();

	/**
	 * Gets the rotational/scaling centre of the surface.
	 * @return The rotational/scaling centre of the surface.
	 */
	Point3d getCentre();

	/**
	 * Applies a 4x4 affine transformation to the surface.
	 * @param matrix The 4x4 affine transformation matrix.
	 */
	void transform(Matrix4d matrix);

	/**
	 * Gets the polyhedral unfolding/net of the projection fitted to the canvas.
	 * @param projections The projections to unfold.
	 * @param w The canvas width.
	 * @param h The canvas height.
	 * @return The polyhedral unfolding/net of the projection.
	 */
	IMesh getProjectionNet(IMesh[][] projections, int w, int h);
}
