package Scene;

import FlaggedProjection.FlaggedProjectionManager;
import FlaggedProjection.ICell;
import FlaggedProjection.IFlaggedProjectionManager;
import Geometry.Polygons.IPolygon2d;
import Mesh.IMesh;
import Mesh.Mesh;
import Polyhedron.ColouredPolyhedron;
import Polyhedron.IColouredPolyhedron;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;
import java.util.ArrayList;
import java.util.List;


/**
 * A 3D scene that contains some screen cells, a 3D model, and its projection onto the cells
 * from the origin.
 */
public class Scene implements IScene {

	/**
	 * The projection screen.
	 */
	protected ISurface m_surface;

	/**
	 * The 3D models to project.
	 */
	protected IColouredPolyhedron[] m_models;

	/**
	 * A 2D list of projections of 3D models onto cells. The (i,j)-th element is the projection of the
	 * j-th 3D model onto the i-th cell.
	 */
	protected IMesh[][] m_projections;

	/**
	 * The vertical field-of-view.
	 */
	protected double m_fovY;

	/**
	 * The position that defines the front of the screen.
	 */
	protected Point3d m_screenFrontPos;

	/**
	 * The vertical up vector for the screen.
	 */
	protected Vector3d m_screenUpVec;

	/**
	 * The manager that helps with flagged projections.
	 */
	protected static IFlaggedProjectionManager FLAGGED_PROJECTION_MANAGER = new FlaggedProjectionManager();

	protected Scene() {}

	/**
	 * Constructs a 3D scene from screen cells, 3D models, and the vertical field-of-view.
	 * @param surface The projection surface.
	 * @param models The models to be projected.
	 * @param fovY The vertical field-of-view.
	 */
	public Scene(ISurface surface, IColouredPolyhedron[] models, double fovY) {

		init(surface, models, fovY);
		recomputeProjection();
	}

	/**
	 * Gets the projection surface.
	 * @return The projection surface.
	 */
	public ISurface getSurface() {
		return m_surface;
	}

	/**
	 * Gets the models.
	 * @return The models.
	 */
	public IColouredPolyhedron[] getModels() {
		return deepCopy(m_models);
	}

	/**
	 * Gets the model specified by the index.
	 * @param index The model index.
	 * @return The model specified by the index.
	 */
	public IColouredPolyhedron getModel(int index) {
		return new ColouredPolyhedron(m_models[index]);
	}

	/**
	 * Sets the model specified by the index.
	 * @param index The index of the model to be set.
	 * @param model The model to set as.
	 */
	public void setModel(int index, IColouredPolyhedron model) {
		m_models[index] = new ColouredPolyhedron(model);
	}

	/**
	 * Gets a copy of the projections.
	 * @return A copy of the projections.
	 */
	public IMesh[][] getProjections() {
		return deepCopy(m_projections);
	}

	/**
	 * Gets the vertical field-of-view.
	 * @return The vertical field-of-view.
	 */
	public double getFovY() {
		return m_fovY;
	}

	/**
	 * Sets the vertical field-of-view.
	 * @param fovY The vertical field-of-view to set as .
	 */
	public void setFovY(double fovY) {
		m_fovY = fovY;
	}

	/**
	 * Gets the position that defines the front of the screen, which is transformed along
	 * with the screen.
	 * @return The position that defines the front of the screen.
	 */
	public Point3d getScreenFrontPos() {
		return new Point3d(m_screenFrontPos);
	}

	/**
	 * Sets the position that defines the front of the screen.
	 * @param p The position that defines the front of the screen.
	 */
	public void setScreenFrontPos(Point3d p) {
		m_screenFrontPos = new Point3d(p);
	}

	/**
	 * Gets the vertical up vector for the screen, which is transformed along
	 * with the screen.
	 * @return The vertical up vector for the screen.
	 */
	public Vector3d getScreenUpVec() {
		return new Vector3d(m_screenUpVec);
	}

	/**
	 * Sets the vertical up vector for the screen.
	 * @param v The vertical up vector for the screen.
	 */
	public void setScreenUpVec(Vector3d v) {
		m_screenUpVec = new Vector3d(v);
	}

	/**
	 * Gets the polyhedral unfolding/net of the screen fitted to the canvas.
	 * @param w The canvas width.
	 * @param h The canvas height.
	 * @return The polyhedral unfolding/net of the screen.
	 */
	public IPolygon2d[] getScreenNet(int w, int h) {
		return m_surface.getUnfoldablePolyhedron().getUnfolding(w, h);
	}

	/**
	 * Gets the polyhedral unfolding/net of the projection fitted to the canvas.
	 * @param w The canvas width.
	 * @param h The canvas height.
	 * @return The polyhedral unfolding/net of the projection.
	 */
	public IMesh getProjectionNet(int w, int h) {
		return m_surface.getProjectionNet(m_projections, w, h);
	}

	/**
	 * Recomputes the projection based on the surface and the models.
	 */
	public void recomputeProjection() {
		ICell[] cells = m_surface.getCells();
		for(int i=0; i<cells.length; i++) {
			m_projections[i] = computeCellProjections(cells[i], m_models);
		}
	}

	/**
	 * Applies a 4x4 affine transformation to the scene.
	 * @param matrix The 4x4 affine transformation matrix.
	 */
	public void transform(Matrix4d matrix) {

		m_surface.transform(matrix);

		for(int j = 0; j< m_models.length; j++) {
			IColouredPolyhedron model = m_models[j];
			model.transform(matrix);
			m_models[j] = model;
		}

		for (int i = 0; i < m_projections.length; i++) {
			IMesh[] cellProjections = m_projections[i];
			for (int j = 0; j < cellProjections.length; j++) {
				IMesh projection = cellProjections[j];
				projection.transform(matrix);
				cellProjections[j] = projection;
			}
			m_projections[i] = cellProjections;
		}

		matrix.transform(m_screenFrontPos);
		matrix.transform(m_screenUpVec);
	}

	/**
	 * Initializes a 3D scene without computing the projections.
	 * @param surface The projection surface.
	 * @param models The models to be projected.
	 * @param fovY The vertical field-of-view.
	 */
	protected void init(ISurface surface, IColouredPolyhedron[] models, double fovY) {
		m_surface = new Surface(surface);
		m_models = deepCopy(models);
		m_projections = new IMesh[m_surface.getNumCells()][m_models.length];

		m_fovY = fovY;
		m_screenFrontPos = new Point3d(0,0,0);
		m_screenUpVec = new Vector3d(0,1,0);
	}

	/**
	 * Computes the projections of a set of models on a single cell.
	 * @param cell The cell.
	 * @param models The models.
	 * @return The projections.
	 */
	private IMesh[] computeCellProjections(ICell cell, IColouredPolyhedron[] models) {
		if(cell == null) return new IMesh[0];
		List<IMesh> projection = new ArrayList<>();
		for(IColouredPolyhedron model : models) {
			IMesh modelMesh = MeshConverter.toMesh(model);
			projection.add(FLAGGED_PROJECTION_MANAGER.projectAndClipToCell(modelMesh, cell));
		}
		return projection.toArray(new IMesh[projection.size()]);
	}

	/**
	 * Makes a deep copy of a set of polyhedra.
	 * @param polyhedra The polyhedra to copy.
	 * @return A deep copy of a set of polyhedra.
	 */
	private IColouredPolyhedron[] deepCopy(IColouredPolyhedron[] polyhedra) {
		int n = polyhedra.length;
		IColouredPolyhedron[] copy = new IColouredPolyhedron[n];
		for(int i=0; i<n; i++) {
			copy[i] = new ColouredPolyhedron(polyhedra[i]);
		}
		return copy;
	}

	/**
	 * Makes a deep copy of a 2D array of meshes.
	 * @param meshes The meshes to copy.
	 * @return A deep copy of a 2D array of meshes.
	 */
	private IMesh[][] deepCopy(IMesh[][] meshes) {
		int n = meshes.length;
		if(n==0) return new IMesh[0][0];
		IMesh[][] copy = new IMesh[n][];
		for(int i=0; i<n; i++) {
			int m = meshes[i].length;
			copy[i] = new IMesh[m];
			for(int j=0; j<m; j++) {
				copy[i][j] = new Mesh(meshes[i][j]);
			}
		}
		return copy;
	}
}
