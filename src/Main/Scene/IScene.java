package Scene;

import Geometry.Polygons.IPolygon2d;
import Mesh.IMesh;
import Polyhedron.IColouredPolyhedron;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import javax.vecmath.Vector3d;

/**
 * A 3D scene that contains some screen cells, a 3D model, and its projection onto the cells
 * from the origin.
 */
public interface IScene {

	/**
	 * Gets the projection surface.
	 * @return The projection surface.
	 */
	ISurface getSurface();

	/**
	 * Gets the models.
	 * @return The models.
	 */
	IColouredPolyhedron[] getModels();

	/**
	 * Gets the model specified by the index.
	 * @param index The model index.
	 * @return The model specified by the index.
	 */
	IColouredPolyhedron getModel(int index);

	/**
	 * Sets the model specified by the index.
	 * @param index The index of the model to be set.
	 * @param model The model to set as.
	 */
	void setModel(int index, IColouredPolyhedron model);

	/**
	 * Gets a copy of the projections.
	 * @return A copy of the projections.
	 */
	IMesh[][] getProjections();

	/**
	 * Gets the vertical field-of-view.
	 * @return The vertical field-of-view.
	 */
	double getFovY();

	/**
	 * Sets the vertical field-of-view.
	 * @param fovY The vertical field-of-view to set as .
	 */
	void setFovY(double fovY);

	/**
	 * Gets the position that defines the front of the screen, which is transformed along
	 * with the screen.
	 * @return The position that defines the front of the screen.
	 */
	Point3d getScreenFrontPos();

	/**
	 * Sets the position that defines the front of the screen.
	 * @param p The position that defines the front of the screen.
	 */
	void setScreenFrontPos(Point3d p);

	/**
	 * Gets the vertical up vector for the screen, which is transformed along
	 * with the screen.
	 * @return The vertical up vector for the screen.
	 */
	Vector3d getScreenUpVec();

	/**
	 * Sets the vertical up vector for the screen.
	 * @param v The vertical up vector for the screen.
	 */
	void setScreenUpVec(Vector3d v);

	/**
	 * Gets the polyhedral unfolding/net of the screen fitted to the canvas.
	 * @param w The canvas width.
	 * @param h The canvas height.
	 * @return The polyhedral unfolding/net of the screen.
	 */
	IPolygon2d[] getScreenNet(int w, int h);

	/**
	 * Gets the polyhedral unfolding/net of the projection fitted to the canvas.
	 * @param w The canvas width.
	 * @param h The canvas height.
	 * @return The polyhedral unfolding/net of the projection.
	 */
	IMesh getProjectionNet(int w, int h);

	/**
	 * Recomputes the projection based on the surface and the models.
	 */
	void recomputeProjection();

	/**
	 * Applies a 4x4 affine transformation to the scene.
	 * @param matrix The 4x4 affine transformation matrix.
	 */
	void transform(Matrix4d matrix);
}