package Scene;

import FlaggedProjection.Cell;
import FlaggedProjection.ICell;
import FlaggedProjection.IPlane;
import FlaggedProjection.Plane;
import Geometry.Lines.ILine3d;
import Geometry.Polygons.IPolygon3d;
import Geometry.Polygons.Polygon3d;
import Mesh.IMesh;
import Mesh.Mesh;
import Polyhedron.IPolyhedron;
import Unfolding.IUnfoldablePolyhedron;
import Unfolding.UnfoldablePolyhedron;

import javax.vecmath.Matrix4d;
import javax.vecmath.Point3d;
import java.util.*;

/**
 * The projection surface containing an underlying polyhedron.
 */
public class Surface implements ISurface {

	/**
	 * The underlying unfoldable polyhedron.
	 */
	private IUnfoldablePolyhedron m_polyhedron;

	/**
	 * The rotational/scaling centre of the surface.
	 */
	private Point3d m_centre;

	/**
	 * Polyhedral faces.
	 */
	private IPolygon3d[] m_faces;

	/**
	 * Triangular projection cells.
	 */
	private ICell[] m_cells;

	/**
	 * The mapping between triangular cells and polyhedral faces.
	 */
	private HashMap<Integer, Integer> m_triangleToPolygonMap;

	/**
	 * Constructor from an unfoldable polyhedron.
	 * @param unfoldablePolyhedron The unfoldable polyhedron.
	 */
	public Surface(IUnfoldablePolyhedron unfoldablePolyhedron) {

		m_polyhedron = new UnfoldablePolyhedron(unfoldablePolyhedron);
		m_centre = CentreHelper.getCentre(m_polyhedron);

		m_faces = unfoldablePolyhedron.getFaces();
		m_cells = toCells(m_faces);
		m_triangleToPolygonMap = getTriangleToPolygonMap(m_faces);
	}

	/**
	 * Copy constructor.
	 * @param surface The surface to copy from.
	 */
	public Surface(ISurface surface) {
		this(surface.getUnfoldablePolyhedron());
	}

	/**
	 * Gets a copy of the underlying unfoldable polyhedron.
	 * @return The underlying unfoldable polyhedron.
	 */
	public IUnfoldablePolyhedron getUnfoldablePolyhedron() {
		return new UnfoldablePolyhedron(m_polyhedron);
	}

	/**
	 * Checks whether or not the surface has an unfolding.
	 * @return Whether or not the surface has an unfolding.
	 */
	public boolean hasUnfolding() {
		return m_polyhedron.getNetTree() != null;
	}

	/**
	 * Gets the faces.
	 * @return The faces.
	 */
	public IPolygon3d[] getFaces() {
		return deepCopy(m_faces);
	}

	/**
	 * Gets the number of faces.
	 * @return The number of faces.
	 */
	public int getNumFaces() {
		return m_faces.length;
	}

	/**
	 * Gets the face specified by the index.
	 * @param index The face index.
	 * @return The face specified by the index.
	 */
	public IPolygon3d getFace(int index) {
		return new Polygon3d(m_faces[index]);
	}

	/**
	 * Gets the triangular projection cells.
	 * @return The triangular projection cells.
	 */
	public ICell[] getCells() {
		return deepCopy(m_cells);
	}

	/**
	 * Gets the number of cells.
	 * @return The number of cells.
	 */
	public int getNumCells() {
		return m_cells.length;
	}

	/**
	 * Gets the rotational/scaling centre of the surface.
	 * @return The rotational/scaling centre of the surface.
	 */
	public Point3d getCentre() {
		return new Point3d(m_centre);
	}

	/**
	 * Applies a 4x4 affine transformation to the surface.
	 * @param matrix The 4x4 affine transformation matrix.
	 */
	public void transform(Matrix4d matrix) {

		m_polyhedron.transform(matrix);
		m_faces = deepCopy(m_polyhedron.getFaces());
		m_cells = toCells(m_faces);
		matrix.transform(m_centre);
	}

	/**
	 * Gets the polyhedral unfolding/net of the projection fitted to the canvas.
	 * @param projections The projections to unfold.
	 * @param w The canvas width.
	 * @param h The canvas height.
	 * @return The polyhedral unfolding/net of the projection.
	 */
	public IMesh getProjectionNet(IMesh[][] projections, int w, int h) {

		if(!hasUnfolding()) return null;

		Matrix4d[] unfoldingTransforms = m_polyhedron.getUnfoldingTransforms(w, h);

		List<IMesh> transformed = new ArrayList<>();

		for(int i=0; i<projections.length; i++) {
			Matrix4d unfoldingTransform = unfoldingTransforms[m_triangleToPolygonMap.get(i)];
			if(unfoldingTransform==null) continue;
			for(IMesh projection : projections[i]) {
				IMesh copy = new Mesh(projection);
				copy.transform(unfoldingTransform);
				transformed.add(copy);
			}
		}

		return new Mesh(transformed.toArray(new IMesh[transformed.size()]));
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) return false;
		if (!ISurface.class.isAssignableFrom(obj.getClass())) return false;
		return getUnfoldablePolyhedron().equals(((ISurface)obj).getUnfoldablePolyhedron());
	}

	/**
	 * Makes a deep copy of the polygons.
	 * @param polygons The polygons.
	 * @return A deep copy of the polygons.
	 */
	private static IPolygon3d[] deepCopy(IPolygon3d[] polygons) {
		int n = polygons.length;
		IPolygon3d[] copy = new IPolygon3d[n];
		for(int i=0; i<n; i++) copy[i] = new Polygon3d(polygons[i]);
		return copy;
	}

	/**
	 * Makes a deep copy of the cells.
	 * @param cells The cells.
	 * @return A deep copy of the cells.
	 */
	private static ICell[] deepCopy(ICell[] cells) {
		int n = cells.length;
		ICell[] copy = new ICell[n];
		for(int i=0; i<n; i++) copy[i] = new Cell(cells[i]);
		return copy;
	}

	/**
	 * Gets the mapping from triangles to faces they belong to, as a result of
	 * the triangulation.
	 * @return The mapping from triangle indices to face indices.
	 */
	private static HashMap<Integer, Integer> getTriangleToPolygonMap(IPolygon3d[] polygons) {

		HashMap<Integer, Integer> map = new HashMap<>();

		int triangleOffset = 0;
		int polygonOffset = 0;

		for(IPolygon3d polygon : polygons) {
			int k = polygon.size()-2;
			for(int i = 0; i<k; i++) {
				map.put(triangleOffset + i, polygonOffset);
			}
			triangleOffset += k;
			polygonOffset++;
		}

		return map;
	}

	/**
	 * Converts the polyhedron's faces into cells, containing a polygon and its outline.
	 * @return The faces as cells.
	 */
	private static ICell[] toCells(IPolygon3d[] polygons) {
		List<ICell> cells = new ArrayList<>();
		for(IPolygon3d polygon : polygons) {
			cells.addAll(toCells(polygon));
		}
		return cells.toArray(new ICell[cells.size()]);
	}

	/**
	 * Converts the polygons to a set of triangular cells.
	 * @return A set of triangulated cells.
	 */
	private static List<ICell> toCells(IPolygon3d polygon) {
		List<ICell> cells = new ArrayList<>();
		int m = polygon.size();
		if(m < 3) return cells;
		else if(m == 3) {
			IPlane plane = new Plane(polygon.get(0), polygon.get(1), polygon.get(2));
			cells.add(new Cell(plane));
		}
		else {
			Point3d[] verts = polygon.getVertices();
			for(int j=3; j<=m; j++) {
				IPlane plane = new Plane(verts[0], verts[j-2], verts[j-1]);
				cells.add(new Cell(plane));
			}
		}
		return cells;
	}

	private static class CentreHelper {

		private static Point3d getCentre(IPolyhedron polyhedron) {

			Set<Point3d> verts = new HashSet<>();
			for (ILine3d edge : polyhedron.getEdges()) {
				verts.add(edge.getP1());
				verts.add(edge.getP2());
			}
			for (IPolygon3d face : polyhedron.getFaces()) {
				verts.addAll(Arrays.asList(face.getVertices()));
			}

			return getCentre(verts.toArray(new Point3d[verts.size()]));
		}

		private static Point3d getCentre(Point3d[] verts) {

			if (verts.length == 0) return null;

			Point3d p0 = verts[0];
			double minX = p0.x, maxX = p0.x;
			double minY = p0.y, maxY = p0.y;
			double minZ = p0.z, maxZ = p0.z;

			for (int i = 1; i < verts.length; i++) {
				Point3d p = verts[i];
				minX = Math.min(minX, p.x);
				minY = Math.min(minY, p.y);
				minZ = Math.min(minZ, p.z);
				maxX = Math.max(maxX, p.x);
				maxY = Math.max(maxY, p.y);
				maxZ = Math.max(maxZ, p.z);
			}

			return new Point3d((minX + maxX) / 2.0, (minY + maxY) / 2.0, (minZ + maxZ) / 2.0);
		}
	}
}
